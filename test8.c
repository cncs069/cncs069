include <stdio.h>

struct student {
    char name[50];
    char sect[50];
    char dept[50];
    int roll;
    int  marks;
} s[10];

int main() 
{
    int i;
    printf("Entering information of students:\n");
    for (i = 0; i < 2; i++) 
    {
        printf("\nEnter roll no.:\n");
        scanf("%d",&s[i].roll);
        printf("Enter student name:\n");
        scanf("%s",s[i].name);
        printf("Enter section:\n");
        scanf("%s",s[i].sect);
        printf("Enter department:\n");
        scanf("%s",s[i].dept);
        printf("Enter marks:\n");
        scanf("%d",&s[i].marks);
       
    }
    int j;
    if (s[0].marks > s[1].marks)
       j = 0;
    else
        j = 1;
    
    printf("\nDisplaying Information:\n");
        printf("\nRoll number: %d\n",s[j].roll);
        printf("Name:");
        puts(s[j].name);
        printf("Section:");
        puts(s[j].sect);
        printf("Department:");
        puts(s[j].dept);
        printf("Marks=%d",s[j].marks);
    
    return 0;
}